db.course_bookings.insertMany([
        {
        	"courseId": "C001", 
        	"studentId": "S004", 
        	"isCompleted": true
        },
        {
        	"courseId": "C002",
        	"studentId": "S001",
        	 "isCompleted": false
        },
        {
        	"courseId": "C001", 
        	"studentId": "S003", 
        	"isCompleted": true
        },
        {
        	"courseId": "C003", 
        	"studentId": "S002", 
        	"isCompleted": false
        },
        {
        	"courseId": "C001", 
        	"studentId": "S002", 
        	"isCompleted": true
        },
        {
        	"courseId": "C004", 
        	"studentId": "S003", 
        	"isCompleted": false
        },
        {
        	"courseId": "C002", 
        	"studentId": "S004", 
        	"isCompleted": true
        },
        {
        	"courseId": "C003", 
        	"studentId": "S007", 
        	"isCompleted": false
        },
        {
        	"courseId": "C001", 
        	"studentId": "S005", 
        	"isCompleted": true
        },
        {
        	"courseId": "C004", 
        	"studentId": "S008", 
        	"isCompleted": false
        },
        {
        	"courseId": "C001", 
        	"studentId": "S013", 
        	"isCompleted": true
        }
]);

//Aggregation in MongoDB
	// This is the act or process of generating manipulated data and perform operations to create filtered results that helps in analyzing data. 
	// This helps in creating reports from analyzing the data provided in our documents. 

/*
	Aggregation Pipeline Syntax: 
		db.collection.aggregate([
			{stage 1},
			{stage 2},
			{stage 3}
		])
 */ 
	
//Aggregation Pipelines
	// Aggregation is done 2-3 steps typically. The first pipeline was with the use of $match. 
	
	// $match - is used to pass the documents or get the documents that will match our condition. 
		// Syntax: {$match:{field:value}}
	//using $match is more like a filter 
	db.course_bookings.aggregate([
			{$match: {"isCompleted": true}},
			{$group:{_id:"$courseId", total:{$sum:1}}}
		]);
	
	//$group - is used to group elements/documents together and create an analysis of these grouped documents. 
		//Syntax: {$group: {id:<id>,fieldResult:"valueResult"}} 
	
	//$project -  this will allow us to show or hide details.
		//Syntax: {$project:{field:0 or 1}} 
			// (1 - show , 0 - hide) 
	
	//$sort - can be used to change the order of the aggregated results.
		//Syntax: {$sort:{field:1/-1}}
			// (1 - assending order, -1 - dessending order)
	
	//$count - allow us to count the total number of items
		//Syntax: {$count: "string"}

//Count all the documents in our collection. 
	
	//using $group
	db.course_bookings.aggregate([
			{$group: {_id:null,count: {$sum:1}}}
		])
	//grouping the documents according to its $fieldName
	db.course_bookings.aggregate([
			{$group: {_id:"$courseId",count: {$sum:1}}} //$sum - use for counting 
		])
	
	//using $project
	db.course_bookings.aggregate([
			{$match: {"isCompleted": true}},
			{$project:{"courseId":0, "studentId":0}}
		]);

	//using $sort
	db.course_bookings.aggregate([
			{$match: {"isCompleted": true}},
			{$sort:{"courseId":-1}}
		]);

/*
	Mini-Activity 
		a. Count the complete
 */
 db.course_bookings.aggregate([
 	{$match:{"studentId": "S013", "isCompleted": true}}
 	{$group:{id: null, count: {$sum: 1 }} }
 	])


	//using $count
	db.course_bookings.aggregate([
	    {$match: {"isCompleted": true}},    
	    {$count: "totalNumberofCompletedCourse"}
	])



db.orders.insertMany([
        {
                "cust_Id": "A123",
                "amount": 500,
                "status": "A"
        },
        {
                "cust_Id": "A123",
                "amount": 250,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "D"
        }
])
	
	db.orders.aggregate([
			{$match: {status:"A"}},
			{$group: {_id: "$cust_Id", total: {$sum:"$amount"}}}
		]);

	//$sum operator will total the values
	//$avg operator will average the results
	//$max operator will show you the highest values
	//$min operator will show you the lowest values
	
/*
	Mini- Activity 
		Get the average amount of the orders per cust_Id. 
 */

	db.orders.aggregate([
			{$group: {_id: "$cust_Id", Average_Amount: {$avg:"$amount"}}}
		]);

/*
	Mini- Activity 
		Get the highest amount of the orders per cust_Id. 
 */

	db.orders.aggregate([
			{$group: {_id: "$cust_Id", High_Amount: {$max:"$amount"}}}
		]);
	
	
	db.orders.aggregate([
			{$match: {"status": {$in:["A","D"]}}},
			{$group: {_id: "$cust_Id", High_Amount: {$max:"$amount"}}}
		]);

